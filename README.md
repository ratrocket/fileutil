# (yet another) fileutil (package)

> **(May 2021)** moved to [md0.org/fileutil](https://md0.org/fileutil). Use `import md0.org/fileutil`

Small Go library of functions to deal with files:

- does something exist at this path
- does this path represent a file
- does this path represent a directory
- is this an empty directory
- etc. (actually right now I think that's about it!)

It's a very thin wrapper around standard library functions that are just
*slightly* cumbersome to deal with or to keep their usage straight after
not using them for a while.
