package fileutil_test

import (
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"testing"

	"md0.org/fileutil"
)

// You'd want to be sure the functions in fileutil work with absolute
// paths, relative paths with "./", relative paths without "./", and
// with directories that end in "/" and don't end in "/".

// testdata layout (f? are files, everything else are dirs)
// Everything except f0 is created in/for the testing process.
// ./d1
// ./d1/e1
// ./d1/e1/f1
// ./d2
// ./d2/f2
// ./d3
// ./f0
// ./file.txt // <-- file with contents "hello, charlie!\n"
// ./more.txt // <-- file with more lines
// ./nonl.txt // <-- file with no final newline
// ./commentsblanks.txt // <-- file with comments and blank lines

const td = "testdata"

// stringsliceEqual tests the equality of two string slices.
// Stolen from package stringslice to remove the dependency.
func stringsliceEqual(s, t []string) bool {
	if len(s) != len(t) {
		return false
	}
	for i, e := range s {
		if e != t[i] {
			return false
		}
	}
	return true
}

func touch(path string) error {
	_, err := os.Create(path)
	return err
}

// returns the first non-nil error in errs
func checkErrs(errs []error) error {
	for _, e := range errs {
		if e != nil {
			return e
		}
	}
	return nil
}

func setupTestData() {
	errs := []error{}
	mode := os.FileMode(0755)
	errs = append(errs, os.Mkdir(filepath.Join(td, "d1"), mode))
	errs = append(errs, os.Mkdir(filepath.Join(td, "d1/e1"), mode))
	errs = append(errs, os.Mkdir(filepath.Join(td, "d2"), mode))
	errs = append(errs, os.Mkdir(filepath.Join(td, "d3"), mode))
	errs = append(errs, touch(filepath.Join(td, "d1/e1/f1")))
	errs = append(errs, touch(filepath.Join(td, "d2/f2")))
	errs = append(errs, ioutil.WriteFile(filepath.Join(td, "file.txt"), []byte("hello, charlie!\n"), 0644))
	errs = append(errs, ioutil.WriteFile(filepath.Join(td, "more.txt"),
		[]byte("hello, charlie!\nhello, sophia!\nhello, circle!\nhello, stuart!\n"), 0644))
	errs = append(errs, ioutil.WriteFile(filepath.Join(td, "nonl.txt"),
		[]byte("one\ntwo\nthree"), 0644))
	// # one
	// // two
	// -blank-
	// content1
	// # three
	// -blank-
	// content2
	// -blank-
	// -blank-
	// # EOF
	errs = append(errs, ioutil.WriteFile(filepath.Join(td, "commentsblanks.txt"),
		[]byte("# one\n// two\n\ncontent1\n# three\n\ncontent2\n\n\n# EOF\n"), 0644))

	if err := checkErrs(errs); err != nil {
		log.Fatal(err)
	}
}

func teardownTestData() {
	errs := []error{}
	errs = append(errs, os.RemoveAll(filepath.Join(td, "d1")))
	errs = append(errs, os.RemoveAll(filepath.Join(td, "d2")))
	errs = append(errs, os.RemoveAll(filepath.Join(td, "d3")))
	errs = append(errs, os.RemoveAll(filepath.Join(td, "file.txt")))
	errs = append(errs, os.RemoveAll(filepath.Join(td, "more.txt")))
	errs = append(errs, os.RemoveAll(filepath.Join(td, "nonl.txt")))
	errs = append(errs, os.RemoveAll(filepath.Join(td, "commentsblanks.txt")))

	if err := checkErrs(errs); err != nil {
		log.Fatal(err)
	}
}

func testdataEmptyExceptF0() bool {
	// we expect there to be an (empty) file in testdata called "f0"
	files, err := ioutil.ReadDir(td)
	if err != nil {
		log.Fatal(err)
	}
	return len(files) == 1 && files[0].Name() == "f0"
}

func TestMain(m *testing.M) {
	if !testdataEmptyExceptF0() {
		log.Fatalf("test data dir (%s) is not as expected", td)
	}
	setupTestData()
	result := m.Run()
	teardownTestData()
	os.Exit(result)
}

func TestIsExist(t *testing.T) {
	tests := []struct {
		path        string
		expected    bool
		addTestdata bool
	}{
		{"d1", true, true},
		{"d1/e1", true, true},
		{"d1/e1/f1", true, true},
		{"d2", true, true},
		{"d2/f2", true, true},
		{"d3", true, true},

		{"n1", false, true},
		{"n2/e1", false, true},
		{"n3/e3/f1", false, true},

		{"/dev/null", true, false},
		{"/usr", true, false},
		{"/usr/", true, false},
		{"..../_/../mysql/", false, false},
		{"/dev/urandom", true, false},
		{"/etc/passwd", true, false},
	}

	for _, test := range tests {
		p := test.path
		if test.addTestdata {
			p = filepath.Join("testdata", p)
		}
		result := fileutil.IsExist(p)
		if result != test.expected {
			t.Errorf("IsExist(%q) = %v; expected %v", test.path, result, test.expected)
		}
	}
}

func TestIsDir(t *testing.T) {
	tests := []struct {
		path        string
		expected    bool
		addTestdata bool
	}{
		{"d1", true, true},
		{"d1/e1", true, true},
		{"d1/e1/f1", false, true},
		{"d2", true, true},
		{"d2/f2", false, true},
		{"d3", true, true},

		{"n1", false, true},
		{"n2/e1", false, true},
		{"n3/e3/f1", false, true},

		{"/dev/null", false, false},
		{"/usr", true, false},
		{"/usr/", true, false},
		{"..../_/../mysql/", false, false},
		{"/dev/urandom", false, false},
		{"/etc/passwd", false, false},
	}

	for _, test := range tests {
		p := test.path
		if test.addTestdata {
			p = filepath.Join("testdata", p)
		}
		result := fileutil.IsDir(p)
		if result != test.expected {
			t.Errorf("IsDir(%q) = %v; expected %v", test.path, result, test.expected)
		}
	}
}

func TestIsFile(t *testing.T) {
	tests := []struct {
		path        string
		expected    bool
		addTestdata bool
	}{
		{"d1", false, true},
		{"d1/e1", false, true},
		{"d1/e1/f1", true, true},
		{"d2", false, true},
		{"d2/f2", true, true},
		{"d3", false, true},

		{"n1", false, true},
		{"n2/e1", false, true},
		{"n3/e3/f1", false, true},

		{"/dev/null", false, false},
		{"/usr", false, false},
		{"/usr/", false, false},
		{"..../_/../mysql/", false, false},
		{"/dev/urandom", false, false},
		{"/etc/passwd", true, false},
	}

	for _, test := range tests {
		p := test.path
		if test.addTestdata {
			p = filepath.Join("testdata", p)
		}
		result := fileutil.IsFile(p)
		if result != test.expected {
			t.Errorf("IsFile(%q) = %v; expected %v", test.path, result, test.expected)
		}
	}
}

func TestIsEmpty(t *testing.T) {
	tests := []struct {
		path        string
		expected    bool
		addTestdata bool
	}{
		{"d1", false, true},
		{"d1/e1", false, true},
		{"d1/e1/f1", false, true},
		{"d2", false, true},
		{"d2/f2", false, true},
		{"d3", true, true},

		{"n1", false, true},
		{"n2/e1", false, true},
		{"n3/e3/f1", false, true},

		{"/dev/null", false, false},
		{"/usr", false, false},
		{"/usr/", false, false},
		{"..../_/../mysql/", false, false},
		{"/dev/urandom", false, false},
		{"/etc/passwd", false, false},
	}

	for _, test := range tests {
		p := test.path
		if test.addTestdata {
			p = filepath.Join("testdata", p)
		}
		result := fileutil.IsEmpty(p)
		if result != test.expected {
			t.Errorf("IsEmpty(%q) = %v; expected %v", test.path, result, test.expected)
		}
	}
}

func TestReadFileLines(t *testing.T) {
	tests := []struct {
		path    string // test will prepend w/ "testdata"
		want    []string
		wantErr bool
		err     error
	}{
		{
			"f0",
			[]string{},
			false,
			nil,
		},
		{
			"file.txt",
			[]string{"hello, charlie!"},
			false,
			nil,
		},
		{
			"more.txt",
			[]string{
				"hello, charlie!",
				"hello, sophia!",
				"hello, circle!",
				"hello, stuart!",
			},
			false,
			nil,
		},
		{
			"nonl.txt",
			[]string{
				"one",
				"two",
				"three",
			},
			false,
			nil,
		},
		{
			"nope.nothing",
			[]string{"doesn't matter"},
			true,
			&os.PathError{},
		},
		{
			"commentsblanks.txt",
			[]string{
				"# one",
				"// two",
				"",
				"content1",
				"# three",
				"",
				"content2",
				"",
				"",
				"# EOF",
			},
			false,
			nil,
		},
	}
	for _, test := range tests {
		got, err := fileutil.ReadFileLines(filepath.Join(td, test.path))

		// didn't want error, didn't get one, BUT results don't match
		if err == nil && !test.wantErr && !stringsliceEqual(got, test.want) {
			t.Errorf("ReadFileLines(%q) = %v (%v), want %v (%v)",
				test.path, got, err, test.want, test.err)
		}

		// didn't get error but expected one
		if err == nil && test.wantErr {
			t.Errorf("ReadFileLines(%q), no err, expected %v", test.path, test.err)
		}

		// got error, didn't expect it
		if err != nil && !test.wantErr {
			t.Errorf("ReadFileLines(%q), go err (%v), didn't expect it", test.path, err)
		}

		// got error, expected it
		if err != nil && test.wantErr {
			// don't know a better way to do this (don't
			// want to use reflection)
			gotErrType := fmt.Sprintf("%T", err)
			wantErrType := fmt.Sprintf("%T", test.err)
			if gotErrType != wantErrType {
				t.Errorf("ReadFileLines(%q) got err type %s, want err type %s",
					test.path, gotErrType, wantErrType)
			}

		}

	}
}

func TestFindFiles(t *testing.T) {
	files, err := fileutil.FindFiles(td)
	if err != nil {
		log.Fatal(err)
	}
	want := []string{
		"testdata/commentsblanks.txt",
		"testdata/d1/e1/f1",
		"testdata/d2/f2",
		"testdata/f0",
		"testdata/file.txt",
		"testdata/more.txt",
		"testdata/nonl.txt",
	}
	if !stringsliceEqual(files, want) {
		// too hard to show the differences, though I do have
		// stringslice.Differences.
		t.Errorf("result not as expected...")
	}
}

func TestMD5(t *testing.T) {
	tests := []struct {
		path string // prepend "testdata" dir
		want string // the md5 of path
	}{
		{
			"f0", // an empty file
			"d41d8cd98f00b204e9800998ecf8427e",
		},
		{
			"file.txt",
			"1a563d85aa22b510552760e608621a47",
		},
	}

	for _, test := range tests {
		got, err := fileutil.MD5(filepath.Join(td, test.path))
		if err != nil {
			log.Fatal(err)
		}
		if got != test.want {
			t.Errorf("MD5(%q) = %s, want %s", test.path, got, test.want)
		}
	}
}

func TestReadFileNonBlankNonCommentLines(t *testing.T) {
	tests := []struct {
		path    string // test will prepend w/ "testdata"
		want    []string
		wantErr bool
		err     error
	}{
		{
			"more.txt",
			[]string{
				"hello, charlie!",
				"hello, sophia!",
				"hello, circle!",
				"hello, stuart!",
			},
			false,
			nil,
		},
		{
			"nonl.txt",
			[]string{
				"one",
				"two",
				"three",
			},
			false,
			nil,
		},
		{
			"nope.nothing",
			[]string{"doesn't matter"},
			true,
			&os.PathError{},
		},
		{
			"commentsblanks.txt",
			[]string{
				"content1",
				"content2",
			},
			false,
			nil,
		},
	}
	for _, test := range tests {
		got, err := fileutil.ReadFileNonBlankNonCommentLines(filepath.Join(td, test.path))

		// didn't want error, didn't get one, BUT results don't match
		if err == nil && !test.wantErr && !stringsliceEqual(got, test.want) {
			t.Errorf("ReadFileNonBlankNonCommentLines(%q) = %v (%v), want %v (%v)",
				test.path, got, err, test.want, test.err)
		}

		// didn't get error but expected one
		if err == nil && test.wantErr {
			t.Errorf("ReadFileNonBlankNonCommentLines(%q), no err, expected %v", test.path, test.err)
		}

		// got error, didn't expect it
		if err != nil && !test.wantErr {
			t.Errorf("ReadFileNonBlankNonCommentLines(%q), go err (%v), didn't expect it", test.path, err)
		}

		// got error, expected it
		if err != nil && test.wantErr {
			// don't know a better way to do this (don't
			// want to use reflection)
			gotErrType := fmt.Sprintf("%T", err)
			wantErrType := fmt.Sprintf("%T", test.err)
			if gotErrType != wantErrType {
				t.Errorf("ReadFileNonBlankNonCommentLines(%q) got err type %s, want err type %s",
					test.path, gotErrType, wantErrType)
			}

		}

	}

}

// Basically a smoke test... tests the corner cases identified in the
// reference I used to write the function.
func TestDetectContentType(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			"f0",
			"text/plain; charset=utf-8",
		},
		{
			"nonl.txt",
			"text/plain; charset=utf-8",
		},
	}

	for _, test := range tests {
		f, err := os.Open(filepath.Join(td, test.name))
		if err != nil {
			t.Fatal(err)
		}
		got, err := fileutil.DetectContentType(f)
		if err != nil {
			t.Fatal(err)
		}
		// fmt.Printf("this: %q\n", got)
		if got != test.want {
			t.Errorf("DetectContentType(%q) = %q, want %q", test.name, got, test.want)
		}
		f.Close()
	}
}

func TestBaseNoext(t *testing.T) {
	tests := []struct {
		name string
		want string
	}{
		{
			"http://example.com/path/to/image1.jpg?xyz",
			"image1",
		},
		{
			"./.config/program/right",
			"right",
		},
		{
			"/.config/program/right.left",
			"right",
		},
		{
			"image1-3600x3600.jpg?key=value",
			"image1-3600x3600",
		},
	}
	for _, test := range tests {
		got := fileutil.BaseNoext(test.name)
		if got != test.want {
			t.Errorf("BaseNoext(%q) = %q, want %q", test.name, got, test.want)
		}
	}
}
