// Package fileutil wraps standard library functions to get info about
// files and directories that is otherwise a little convoluted to work
// out every time it's needed.
//
// In the future the package may add functions to help manipulate files
// and directories.
package fileutil

import (
	"crypto/md5"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"regexp"
	"strings"
)

// IsExist returns true if p is a path to an existing file or directory.
func IsExist(p string) bool {
	return exists0(p)

	// This does as well as exists0()...
	// _, err := os.Stat(p)
	// return err == nil
}

// IsDir returns true if p is a path to an existing directory.
func IsDir(p string) bool {
	// I feel like this is "safer" than the option below.
	if !IsExist(p) {
		return false
	}
	info, _ := os.Stat(p)
	return info.IsDir()

	// info, err := os.Stat(p)
	// return err == nil && info.IsDir()
}

// IsFile returns true if p is a path to an existing regular file.
func IsFile(p string) bool {
	// Like in IsDir, I feel like this is "safer".
	if !IsExist(p) {
		return false
	}
	info, _ := os.Stat(p)
	return info.Mode().IsRegular()

	// info, err := os.Stat(p)
	// return err == nil && info.Mode().IsRegular()
}

// IsEmpty returns true if p is a dir and contains no files.
func IsEmpty(p string) bool {
	// It's a little bit fast-n-loose by not first checking if
	// IsDir(p)==true, but...
	files, err := ioutil.ReadDir(p)
	return err == nil && len(files) == 0
}

// ReadFileLinesDie wraps ioutil.ReadFile with the additions of
// returning the lines of the file p as a string slice, and it takes
// care of getting rid of the final "" element caused by the final
// newline in the file. And, it dies if it encounters an error. This is
// obviously only for situations where that's appropriate, like solving
// puzzles.
func ReadFileLinesDie(p string) []string {
	lines, err := ReadFileLines(p)
	if err != nil {
		log.Fatalf("fileutil.ReadFileLinesDie: %v", err)
	}
	return lines
}

// ReadFileLines is an error-returning version of ReadFileLinesDie.  See
// description of ReadFileLinesDie for details.
func ReadFileLines(p string) ([]string, error) {
	data, err := ioutil.ReadFile(p)
	if err != nil {
		return []string{}, err
	}
	lines := strings.Split(string(data), "\n")
	if lines[len(lines)-1] == "" {
		lines = lines[:len(lines)-1]
	}
	return lines, nil
}

// ReadFileNonBlankNonCommentLines is like the above ReadFileLines
// functions, but ignores blank lines and lines starting with "#" and
// "//".
func ReadFileNonBlankNonCommentLines(p string) ([]string, error) {
	lines := []string{}
	raws, err := ReadFileLines(p)
	if err != nil {
		return lines, err
	}
	for _, raw := range raws {
		if strings.HasPrefix(raw, "#") || strings.HasPrefix(raw, "//") {
			continue
		}
		matched, err := regexp.MatchString(`^\s*$`, raw)
		if err != nil {
			return []string{}, fmt.Errorf("regexp.MatchString err on %q: %v", raw, err)
		}
		if matched {
			continue
		}
		lines = append(lines, raw)
	}
	return lines, nil
}

// ReadFileNonBlankNonCommentLinesDie is a panicing version of
// ReadFileNonBlankNonCommentLines.
func ReadFileNonBlankNonCommentLinesDie(p string) []string {
	lines, err := ReadFileNonBlankNonCommentLines(p)
	if err != nil {
		log.Fatalf("fileutil.ReadFileNonBlankNonCommentLinesDie: %v", err)
	}
	return lines
}

// FindFiles recursively finds (or lists) all files under base.
// Directories are ignored.  It's like `find base type -f`.
// NOTE: This *will* return "hidden" files (files starting with a dot).
// I'm not sure if that's desireable, but for now the caller is
// responsible for filtering those out if they aren't wanted.
func FindFiles(base string) ([]string, error) {
	files := []string{}
	err := filepath.Walk(base, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		files = append(files, path)
		return nil
	})
	if err != nil {
		return []string{}, err
	}
	return files, nil
}

// Stolen from Russ Cox "Error Handling — Problem Overview"
// https://go.googlesource.com/proposal/+/master/design/go2draft-error-handling-overview.md
//
// But also cf. CopyFile from Dave Cheney's github.com/pkg/fileutils
func CopyFile(src, dst string) error {
	r, err := os.Open(src)
	if err != nil {
		return fmt.Errorf("copy %s %s: %v", src, dst, err)
	}
	defer r.Close()

	w, err := os.Create(dst)
	if err != nil {
		return fmt.Errorf("copy %s %s: %v", src, dst, err)
	}

	if _, err := io.Copy(w, r); err != nil {
		w.Close()
		os.Remove(dst)
		return fmt.Errorf("copy %s %s: %v", src, dst, err)
	}

	if err := w.Close(); err != nil {
		os.Remove(dst)
		return fmt.Errorf("copy %s %s: %v", src, dst, err)
	}
	return nil
}

func MD5(path string) (string, error) {
	f, err := os.Open(path)
	if err != nil {
		return "", err
	}
	defer f.Close()
	h := md5.New()
	if _, err := io.Copy(h, f); err != nil {
		return "", err
	}
	return fmt.Sprintf("%x", h.Sum(nil)), nil
}

// Detect content-type without having the whole file in memory.
//
// comment from reference:
// At most the first 512 bytes of data are used:
// https://golang.org/src/net/http/sniff.go?s=646:688#L11
//
// ref:
// https://gist.github.com/rayrutjes/db9b9ea8e02255d62ce2
// https://archive.is/iGqI9
func DetectContentType(rs io.ReadSeeker) (string, error) {
	b := make([]byte, 512)
	n, err := rs.Read(b)
	if err != nil && err != io.EOF {
		return "", err
	}
	// not sure this is nec
	rs.Seek(0, io.SeekStart)
	return http.DetectContentType(b[:n]), nil
}

// BaseNoext returns the basename of path (ie, the filename without the
// preceding path components) without the extension (what *I* sometimes
// erroneously think of as the "basename").
//
// NB: uses package path, so meant to work on slash-separated paths.
func BaseNoext(name string) string {
	return strings.TrimSuffix(path.Base(name), path.Ext(name))
}

// I'm not sure functions like this are a good idea, generally.  I think
// it's better to wrap the library function in your own package with a
// "Must" version if you need it.  But who knows!
// func MustMD5(path string) string {
// 	f, err := os.Open(path)
// 	if err != nil {
// 		log.Fatal(err)
// 	}
// 	defer f.Close()
// 	h := md5.New()
// 	if _, err := io.Copy(h, f); err != nil {
// 		log.Fatal(err)
// 	}
// 	return fmt.Sprintf("%x", h.Sum(nil))
// }

// Different versions of "exists":

// Summary of exists0, exists1, exists2:
//
// It doesn't seem to be important for my purposes to retrieve
// the error from os.Stat, so going with the simplest exists*
// function seems fine.

// MOST SIMPLE (but less info; an error returned by os.Stat will be
// interpreted by the caller as the [months later, I have no idea how I
// was going to finish that sentence...])
func exists0(path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return false
		}
	}
	return true
}

// MORE SIMPLE (but less info)
// if error is nil, file exists
// else *either* file doesn't exist,
//   *or* there was an error Stat-ing the file (no way to tell which...)
//
// Using:
// err := exists1(p)
// if err == nil {
// 	return true
// }
// return false
// OR
// return exists1(p) == nil
func exists1(path string) error {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			return errors.New("file does not exist")
		} else {
			return err // other error
		}
	}
	return nil
}

// LEAST SIMPLE
//
// Using (if you don't care about the error):
// ok, _ := exists2(p)
// return ok
func exists2(path string) (bool, error) {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			// file does not exist
			return false, nil
		} else {
			// other error
			return false, err
		}
	}
	return true, nil
}
